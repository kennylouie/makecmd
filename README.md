# makecmd op

This @cto.ai/op enters a directory inside the shared state directory shared between ops in a @cto.ai/op workflow and executes a make command. Standard out and error will be displayed to the interface.

This op can be run in the cli (using the @cto.ai tool https://cto.ai/docs/getting-started) or over slack (with the @cto.ai slackbot https://cto.ai/docs/slackapp)

# To use in a workflow:
+ Add to the ops.yml steps e.g:
```
steps:
	- ops run @kennylouie/makecmd:[LATEST_VERSION] --dir [DIRECTORY] --cmd [YOUR_COMMAND]
	- ...

```
# Building from source and running

The value of the op comes from being used with a user-specific case. Add into the `Dockerfile` any deps needed by your make command.
E.g.
```
RUN: apt install -y python3
```

+ clone this repo
+ ensure @cto.ai/ops is installed
+ build the op
```
ops build .
```

+ publish the op to your team that is associated with the slackbot
```
ops publish .
```
