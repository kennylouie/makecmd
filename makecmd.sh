#!/bin/bash

set -u

main()
{
	need_cmd bash-sdk
	need_cmd jq
	need_cmd make
	need_cmd stdbuf

	local _dir _cmd
	get_dir
	get_cmd

	ensure cd "${SDK_STATE_DIR}/${_dir}" && \
		ensure stdbuf -oL make "${_cmd}" | \
		while IFS= read -r line
		do
			say "${line}"
		done

	say Done!
}

get_dir()
{
	_dir="$(bash-sdk prompt input -a \
		--message "Directory name" \
		--name "dir" | jq --raw-output ".dir")"

	check_empty "${_dir}"
}

get_cmd()
{
	_cmd="$(bash-sdk prompt input -a \
		--message "make command" \
		--name "cmd" \
		--default-value "" | jq --raw-output ".cmd")"

	check_empty "${_cmd}"
}

ensure()
{
	if ! "$@"; then err "command failed: $*"; fi
}

check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	bash-sdk print "$(printf "%b" "@makecmd: $@\n")"
}

main "$@"
