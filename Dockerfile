FROM registry.cto.ai/official_images/bash:latest

WORKDIR /ops

RUN apt update && apt install -y jq make

# Add additional deps for your own op
RUN apt install -y gcc

ADD makecmd.sh .
